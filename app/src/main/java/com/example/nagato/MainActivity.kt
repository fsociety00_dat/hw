package com.example.nagato

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        generateRandomNumberButton.setOnClickListener {
            val randnum:Int = randomNumber()
            d("ButtonClicked", " $randnum")

        randomNumberTextView.text = randnum.toString()
        if (randnum%5==0 && randnum>0) {numberResult.text = "Yes" }
            else {numberResult.text = "No"}

        }

    }

    private fun randomNumber():Int = (-100..100).random()



}
